import React from 'react'
  
const OutputScreenRow = () => {
  return (
    <div className="screen-row">
      {/* input is readOnly because we do not want to be able to modify from here */}
      <input type="text" readOnly/>
    </div>
  )
}

export default OutputScreenRow